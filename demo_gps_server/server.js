var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var gpsList = [
    {lat: '32.776664', lng: '-96.796988'},
    {lat: '32.775224', lng: '-96.812753'},
    {lat: '32.772192', lng: '-96.841335'},
    {lat: '32.773203', lng: '-96.87335'},
    {lat: '32.770749', lng: '-96.902189'},
    {lat: '32.766274', lng: '-96.935491'},
    {lat: '32.764326', lng: '-96.962699'},
    {lat: '32.762738', lng: '-96.983985'}
];
var startIndex = 0;


app.get('/', function(req, res) {
  res.send('Sample GPS Server');
});

app.get('/generate_gps', function(req,res){
   //Set gps coordinate to the next coordinate
   console.log('generate gps');
   var gpsCoord = gpsList[startIndex];
   gpsCoord.tagTitle = 'Drone Tagged Point '+startIndex;

   io.emit('gps_coord',gpsCoord);

   startIndex++;
   if(startIndex == gpsList.length){
        startIndex = 0;
   }
   res.send('sucess');

});

var server = http.listen(4000, function () {
  var host = server.address().address
  var port = server.address().port
  console.log('GPS Server app listening at http://%s:%s', host, port)
});


//Socket io functions
io.on('connection', function(socket){
  console.log('Maps Client has connected');
});